<?php
	
	function GetAllProfiles() {
		$sql = "SELECT * FROM air_perfil_usuario";
		
		$stmt = getConn()->query($sql);
		$perfis = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		echo json_encode($perfis);
	}

	function GetProfileId($id) {
		$sql = "SELECT * FROM air_perfil_usuario WHERE id = :id";
		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id",$id);
		$stmt->execute();
		
		$perfil = $stmt->fetchObject();

		echo json_encode($perfil);
	}

	function SaveProfile(){
		$request = \Slim\Slim::getInstance()->request();
		$perfil = json_decode($request->getBody());

		$sql = "INSERT INTO air_perfil_usuario (nome,descricao,qtdUsuariosVinculados) values
		 (:nome,:descricao,:qtdUsuariosVinculados) ";

		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("nome",$perfil->nome);
		$stmt->bindParam("descricao",$perfil->descricao);
		$stmt->bindParam("qtdUsuariosVinculados",$perfil->qtdUsuariosVinculados);

		$stmt->execute();
		$perfil->id = $conn->lastInsertId();
		echo json_encode($perfil);
	}

	function UpdateProfile($id){
		$request = \Slim\Slim::getInstance()->request();
		$perfil = json_decode($request->getBody());
		
		$sql = "UPDATE air_perfil_usuario SET nome=:nome, descricao=:descricao, qtdUsuariosVinculados=:qtdUsuariosVinculados WHERE id=:id";

		$conn = getConn();
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("nome",$perfil->nome);
		$stmt->bindParam("descricao",$perfil->descricao);
		$stmt->bindParam("qtdUsuariosVinculados",$perfil->qtdUsuariosVinculados);
		$stmt->bindParam("id",$id);
		$stmt->execute();

		echo json_encode($perfil);
	}

	function SaveAccessProfiles(){

		$request = \Slim\Slim::getInstance()->request();
		$vinculo = json_decode($request->getBody());

		if($vinculo->id == 0) {
			$sql = "INSERT INTO air_menu_perfil (id_menu, id_perfil, ativo) VALUES (:id_menu, :id_perfil, :ativo) ";

			$conn = getConn();
			$stmt = $conn->prepare($sql);

			$stmt->bindParam("id_menu",$vinculo->id_menu);
			$stmt->bindParam("id_perfil",$vinculo->id_perfil);
			$stmt->bindParam("ativo",$vinculo->ativo);
			$stmt->execute();

			echo json_encode($vinculo);
		} else {
			$sql = "UPDATE air_menu_perfil set ativo = :ativo where id = :id ";

			$conn = getConn();
			$stmt = $conn->prepare($sql);

			$stmt->bindParam("ativo",$vinculo->ativo);
			$stmt->bindParam("id",$vinculo->id);
			$stmt->execute();
		}
	}

	function GetAccessUserProfile($idProfile) {
		$sql = "SELECT am.air_menu_pai_id,
				       am.icon,
				       mp.id,
				       am.nome,
				       am.url,
				       mp.ativo
		          FROM air_menu am
		          JOIN air_menu_perfil mp on am.id = mp.id_menu
		          JOIN air_perfil_usuario pu on pu.id = mp.id_perfil
		          JOIN pessoa pe on pe.perfil_id = pu.id
		         WHERE pe.id = :id
		           AND mp.ativo = 'S'";


		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id",$idProfile);
		$stmt->execute();
		
		$acessos = $stmt->fetchAll(PDO::FETCH_OBJ);

		echo json_encode($acessos);
	}

	function GetAccessProfile($idProfile) {

		if($idProfile != 0) {
			$sql = "SELECT am.air_menu_pai_id,
					       am.icon,
					       mp.id,
					       am.nome,
					       am.url,
					       mp.ativo
			          FROM air_menu am
			     left JOIN air_menu_perfil mp on am.id = mp.id_menu
			     left JOIN air_perfil_usuario pu on pu.id = mp.id_perfil
			         WHERE pu.id = :id";


			$conn = getConn();		
			$stmt = $conn->prepare($sql);

			$stmt->bindParam("id",$idProfile);
			$stmt->execute();
			
			$acessos = $stmt->fetchAll(PDO::FETCH_OBJ);

			echo json_encode($acessos);
	     } else {
	     	$sql = "SELECT am.air_menu_pai_id,
					       am.icon,
					       am.id,
					       am.nome,
					       am.url,
					       am.ativo
			          FROM air_menu am";

			$conn = getConn();		
			$stmt = $conn->prepare($sql);

			// $stmt->bindParam("id",$idProfile);
			$stmt->execute();
			
			$acessos = $stmt->fetchAll(PDO::FETCH_OBJ);

			echo json_encode($acessos);
	     }
	}

	function DeleteProfileSelected($id) {

		$sql_1 = "DELETE FROM air_menu_perfil WHERE id_perfil = :id ";
		$sql_2 = "DELETE FROM air_perfil_usuario WHERE id = :id ";
		
		$conn = getConn();
		$stmt = $conn->prepare($sql_1);
		$stmt->bindParam("id",$id);
		$stmt->execute();

		$conn = getConn();
		$stmt = $conn->prepare($sql_2);
		$stmt->bindParam("id",$id);
		$stmt->execute();
	}
?>