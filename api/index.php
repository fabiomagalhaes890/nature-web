<?php
	require '../../Slim/Slim/Slim.php';
	
	include("Login.php");	
	include("Estados.php");	
	include("Cidades.php");	
	include("Contato.php");	
	include("Tarefa.php");	
	include("Perfis.php");
	include("Pessoa.php");

	\Slim\Slim::registerAutoloader();
	
	$app = new \Slim\Slim();
	$app->response()->header('Content-Type', 'application/json;charset=utf-8');

	$app->post('/login-air', 'LoginUser');
	$app->post('/logout-air', 'Logout');
	$app->post('/validar-conexao', 'ValidarConexao');

	// Estados
	$app->get('/estados', 'TodosEstados');
	$app->get('/estados/:id','PesquisarEstadosId');
	// End Estados

	$app->post('/enviar-email', 'EnviarEmail');
	$app->post('/enviar-email-calculo', 'EnvioEmailCalculo');

	// Cidades
	$app->get('/cidades', 'TodasCidades');
	$app->get('/cidades/:id','PesquisarCidadeId');
	$app->get('/cidades/estado/:id','PesquisaCidadePorEstado');
	// End Cidades

	$app->get('/tarefas/usuario/:id', 'BuscarTarefasUsuario');

	$app->get('/perfil-de-acesso', 'GetAllProfiles');
	$app->get('/perfil-de-acesso/:id','GetProfileId');
	$app->put('/perfil-de-acesso/:id','UpdateProfile');
	$app->post('/perfil-de-acesso', 'SaveProfile');

	$app->get('/perfil/acessos/:id','GetAccessProfile');
	$app->get('/acessos/login/:id', 'GetAccessUserProfile');
	$app->post('/perfil/acessos','SaveAccessProfiles');
	$app->delete('/excluir-perfil/:id', 'DeleteProfileSelected');

	$app->get('/pessoas', 'BuscarPessoas');
	$app->get('/pessoas/:id', 'BuscarPessoaId');
	$app->post('/pessoas', 'SalvarPessoa');
	$app->put('/pessoas/:id', 'AlterarPessoa');
	$app->delete('/pessoas/:id', 'ExcluirPessoa');

	$app->get('/quantidade-perfil/:id', 'AlteraQuantidadePerfil');

	$app->run();

	function getConn()
	{
		return new PDO('mysql:host=oxigeniocomunicacao.com.br;dbname=oxigenio_air',
			'oxigenio_air',
			'Tj9lM#sv',
			array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
		);
		// return new PDO('mysql:host=localhost;dbname=oxigenio_air',
		// 	'root',
		// 	'',
		// 	array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
		// );
	}
?>