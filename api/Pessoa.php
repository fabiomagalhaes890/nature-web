<?php

	function BuscarPessoas() {
		$sql = "SELECT * FROM pessoa WHERE perfil = 'Oxigênio'";
		
		$stmt = getConn()->query($sql);
		$pessoas = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		echo json_encode($pessoas);
	}

	function BuscarPessoaId($id) {
		$sql = "SELECT * FROM pessoa WHERE id = :id";
		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id",$id);
		$stmt->execute();
		
		$pessoas = $stmt->fetchObject();

		echo json_encode($pessoas);
	}

	function AlterarPessoa($id) {
		$request = \Slim\Slim::getInstance()->request();
		$pessoa = json_decode($request->getBody());

		$dataalteracao = date("Y-m-d");
		$perfilPadrao = 'Oxigênio';
		$statusPadrao = 1;
		$permissao = 0;

		$sql = "UPDATE pessoa 
				   SET id_cliente= :id_cliente,
				       tipo_pessoa= :tipo_pessoa,
				       perfil= :perfil,
				       permissao= :permissao,
				       nome_fantasia= :nome_fantasia,
				       nome_real= :nome_real,
				       email= :email,
				       senha= :senha,
				       numero_fiscal= :numero_fiscal,
				       numero_registro= :numero_registro,
				       telefone= :telefone,
				       celular= :celular,
				       endereco= :endereco,
				       numero= :numero,
				       complemento= :complemento,
				       bairro= :bairro,
				       cep= :cep,
				       cidade= :cidade,
				       estado= :estado,
				       data_alteracao= :data_alteracao,
				       status= :status,
				       perfil_id= :perfil_id 
				WHERE id = :id";

		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id_cliente",$pessoa->id_cliente);
		$stmt->bindParam("tipo_pessoa",$pessoa->tipo_pessoa);
		$stmt->bindParam("perfil",$perfilPadrao);
		$stmt->bindParam("permissao",$permissao);
		$stmt->bindParam("nome_fantasia",$pessoa->nome_fantasia);
		$stmt->bindParam("nome_real",$pessoa->nome_real);
		$stmt->bindParam("email",$pessoa->email);
		$stmt->bindParam("senha",$pessoa->senha);
		$stmt->bindParam("numero_fiscal",$pessoa->numero_fiscal);
		$stmt->bindParam("numero_registro",$pessoa->numero_registro);
		$stmt->bindParam("telefone",$pessoa->telefone);
		$stmt->bindParam("celular",$pessoa->celular);
		$stmt->bindParam("endereco",$pessoa->endereco);
		$stmt->bindParam("numero",$pessoa->numero);
		$stmt->bindParam("complemento",$pessoa->complemento);
		$stmt->bindParam("bairro",$pessoa->bairro);
		$stmt->bindParam("cep",$pessoa->cep);
		$stmt->bindParam("cidade",$pessoa->cidade);
		$stmt->bindParam("estado",$pessoa->estado);
		$stmt->bindParam("data_alteracao",$dataalteracao);
		$stmt->bindParam("status",$statusPadrao);
		$stmt->bindParam("perfil_id",$pessoa->perfil_id);
		$stmt->bindParam("id",$id);

		$stmt->execute();
	}

	function SalvarPessoa() {
		$request = \Slim\Slim::getInstance()->request();
		$pessoa = json_decode($request->getBody());

		$datacadastro = date("Y-m-d");
		$perfilPadrao = 'Oxigênio';
		$statusPadrao = 1;
		$permissao = 0;

		$sql = "INSERT INTO pessoa(id_cliente, tipo_pessoa, perfil, permissao, nome_fantasia, 
			                       nome_real, email, senha, numero_fiscal, numero_registro, 
			                       telefone, celular, endereco, numero, complemento, bairro, cep, 
			                       cidade, estado, data_cadastro, status, perfil_id) VALUES 
                                  (:id_cliente,:tipo_pessoa,:perfil,:permissao,:nome_fantasia,
                                   :nome_real,:email,:senha,:numero_fiscal,:numero_registro,
                                   :telefone,:celular,:endereco,:numero,:complemento,:bairro,:cep,
                                   :cidade,:estado,:data_cadastro,:status,:perfil_id) ";

		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id_cliente",$pessoa->id_cliente);
		$stmt->bindParam("tipo_pessoa",$pessoa->tipo_pessoa);
		$stmt->bindParam("perfil",$perfilPadrao);
		$stmt->bindParam("permissao",$permissao);
		$stmt->bindParam("nome_fantasia",$pessoa->nome_fantasia);
		$stmt->bindParam("nome_real",$pessoa->nome_real);
		$stmt->bindParam("email",$pessoa->email);
		$stmt->bindParam("senha",$pessoa->senha);
		$stmt->bindParam("numero_fiscal",$pessoa->numero_fiscal);
		$stmt->bindParam("numero_registro",$pessoa->numero_registro);
		$stmt->bindParam("telefone",$pessoa->telefone);
		$stmt->bindParam("celular",$pessoa->celular);
		$stmt->bindParam("endereco",$pessoa->endereco);
		$stmt->bindParam("numero",$pessoa->numero);
		$stmt->bindParam("complemento",$pessoa->complemento);
		$stmt->bindParam("bairro",$pessoa->bairro);
		$stmt->bindParam("cep",$pessoa->cep);
		$stmt->bindParam("cidade",$pessoa->cidade);
		$stmt->bindParam("estado",$pessoa->estado);
		$stmt->bindParam("data_cadastro", $datacadastro);
		$stmt->bindParam("status",$statusPadrao);
		$stmt->bindParam("perfil_id",$pessoa->perfil_id);

		$stmt->execute();
		$pessoa->id = $conn->lastInsertId();
		echo json_encode($pessoa);
	}

	function ExcluirPessoa($id) {

		$sql_1 = "DELETE FROM pessoa WHERE id = :id ";
		
		$conn = getConn();
		$stmt = $conn->prepare($sql_1);
		$stmt->bindParam("id",$id);
		$stmt->execute();
	}

	function AlteraQuantidadePerfil($id) {

		$query = " SELECT COUNT(*) AS total FROM pessoa WHERE perfil_id = :id ";

		$conn = getConn();
		$stmt = $conn->prepare($query);
		$stmt->bindParam("id",$id);
		$stmt->execute();

		$quantidade = $stmt->fetchObject();

		$sql = " UPDATE air_perfil_usuario 
		            SET qtdUsuariosVinculados = :quantidade 
		          WHERE id = :id ";

		$conn = getConn();
		$stmt = $conn->prepare($sql);
		$stmt->bindParam("quantidade",$quantidade->total);
		$stmt->bindParam("id",$id);
		$stmt->execute();
	}
?>