<?php
	
	function TodasCidades(){
		$sql = "SELECT * FROM cidade";
		
		$stmt = getConn()->query($sql);
		$cidades = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		echo json_encode($cidades);
	}

	function PesquisarCidadeId($id) {
		$sql = "SELECT * FROM cidade WHERE ID = :id";
		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id",$id);
		$stmt->execute();
		
		$cidade = $stmt->fetchObject();

		echo json_encode($cidade);
	}

	function PesquisaCidadePorEstado($id) {
		$sql = "SELECT * FROM cidade WHERE ESTADO_ID = :id";
		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id",$id);
		$stmt->execute();

		$cidades = $stmt->fetchAll(PDO::FETCH_OBJ);
		echo json_encode($cidades);
	}
?>