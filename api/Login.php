<?php

	function LoginUser() {

		include("classes/Pessoa.php");
		$userLogged = new Pessoa;

		$request = \Slim\Slim::getInstance()->request();
        $userlogin = json_decode($request->getBody());

        $sql = "SELECT * FROM pessoa WHERE email = :email AND senha = :senha";
		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("email",$userlogin->email);
		$stmt->bindParam("senha",$userlogin->senha);
		$stmt->execute();
		
		$userlogin = $stmt->fetchObject();
		
		$better_token = md5(uniqid(rand(), true));		
		$retorno = $userLogged->PessoaConstructor($userlogin->id, $better_token, $userlogin->nome_fantasia, $userlogin->perfil_id);

		$sqlupdate = "UPDATE pessoa SET uid = :uid WHERE id = :id";
		$connup = getConn();		
		$stmtup = $connup->prepare($sqlupdate);

		$stmtup->bindParam("uid",$retorno->token);
		$stmtup->bindParam("id",$retorno->idUser);
		$stmtup->execute();

		if($userlogin)
			echo json_encode($retorno);
		else
			echo json_encode(false);
	}

	function Logout() {

		$request = \Slim\Slim::getInstance()->request();
        $userlogin = json_decode($request->getBody());

		$sqlupdate = "UPDATE pessoa SET uid = NULL WHERE id = :id";
		$connup = getConn();		
		$stmtup = $connup->prepare($sqlupdate);

		$stmtup->bindParam("id",$userlogin->idUser);
		$stmtup->execute();

		if($userlogin)
			echo json_encode(true);
		else
			echo json_encode(false);	
	}

	function ValidarConexao() {

		$request = \Slim\Slim::getInstance()->request();
        $userlogin = json_decode($request->getBody());

        $sql = "SELECT * FROM pessoa WHERE id = :id_user AND uid = :token";

        $connup = getConn();		
		$stmtup = $connup->prepare($sql);

		$stmtup->bindParam("id_user",$userlogin->idUser);
		$stmtup->bindParam("token",$userlogin->token);
		$validado = $stmtup->execute();

		if($validado)
			echo json_encode(true);
		else
			echo json_encode(false);
	}

?>