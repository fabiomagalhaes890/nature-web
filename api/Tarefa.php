<?php 
	
	function BuscarTarefasUsuario($id) {
		
		$sql = "SELECT pt.id, 
		               pt.id_projeto_cliente, 
		               pt.id_projeto, 
		               pt.id_pessoa, 
		               pt.tipo_tarefa, 
		               pt.situacao, 
		               pt.data_inicio, 
		               pt.data_entrega, 
		               pt.valor_tarefa, 
		               pt.horas_tarefa, 
		               pt.titulo,
        			   pp.projeto, 
        			   p.nome_fantasia cliente, 
        			   pr.nome_fantasia responsavel, 
        			   pc.id_projeto, 
        			   pc.id_pessoa id_cliente,
        			   (SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( pth.horas_tarefa ) ) ) 
        			   	  FROM projeto_tarefa_horas pth 
        			   	 WHERE pt.id = pth.id_tarefa) horas_gasto
                  FROM projeto_tarefa pt
            INNER JOIN projeto_cliente pc ON pc.id = pt.id_projeto_cliente
            INNER JOIN projeto pp ON pp.id = pc.id_projeto
            INNER JOIN pessoa p ON p.id = pc.id_pessoa
            INNER JOIN pessoa pr ON pr.id = pt.id_pessoa
                 WHERE pc.status = 1 
                   AND (pt.situacao <> 'Finalizado' AND pt.situacao <> 'Aguardando' AND pt.situacao <> 'Teste')
                   AND pt.id_pessoa = :id
                 ORDER BY pr.nome_fantasia, pt.situacao, pt.data_entrega ASC";

        $conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id",$id);
		$stmt->execute();

		$tarefas = $stmt->fetchAll(PDO::FETCH_OBJ);
		echo json_encode($tarefas);
	}
?>