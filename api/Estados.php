<?php
	
	function TodosEstados(){
		$sql = "SELECT * FROM estado";
		
		$stmt = getConn()->query($sql);
		$estados = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		echo json_encode($estados);
	}

	function PesquisarEstadosId($id) {
		$sql = "SELECT * FROM estado WHERE ID = :id";
		$conn = getConn();		
		$stmt = $conn->prepare($sql);

		$stmt->bindParam("id",$id);
		$stmt->execute();
		
		$estado = $stmt->fetchObject();

		echo json_encode($estado);
	}
?>