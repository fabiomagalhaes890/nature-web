var app = angular.module('oxiapp', ['ngCookies','ngRoute', 'ngResource']);
	app.constant("apiUrl",(function() {
        // var resource = 'http://oxigeniocomunicacao.com.br/zimba/api/';
        var resource = 'api/';
            return {
                LOGIN: resource + "login-air",
                LOGOUT: resource + "logout-air",
                VALIDAR: resource + "validar-conexao",
                TAREFAS: resource + "tarefas/usuario/",
                SALVAR_PERFIL:  resource + "perfil-de-acesso",
                ALTERAR_PERFIL: resource + "perfil-de-acesso/",
                CARREGAR_PERFIS: resource + "perfil-de-acesso",
				CARREGAR_ACESSOS: resource + "perfil/acessos/",
				SAVE_UPDATE_ACESSOS: resource + "perfil/acessos",
				CARREGAR_ACESSOS_LOGIN: resource + "acessos/login/",
				EXCLUIR_PERFIL: resource + "excluir-perfil/",
				BUSCAR_PESSOAS: resource + "pessoas",
				QUANTIDADE: resource + "quantidade-perfil/"
            }
      })());
	app.config(function($routeProvider, $locationProvider) {

		$routeProvider.when('/usuarios', {
			templateUrl: 'partials/listagem-usuarios.html',
			controller: 'ListagemUsuariosController'
		});

		$routeProvider.when('/usuarios/cadastro', {
			templateUrl: 'partials/cadastro-usuarios.html',
			controller: 'CadastroUsuariosController'
		});

		$routeProvider.when('/usuarios/cadastro/:id', {
			templateUrl: 'partials/cadastro-usuarios.html',
			controller: 'CadastroUsuariosController'
		});

		$routeProvider.when('/perfil-de-acesso', {
			templateUrl: 'partials/perfil-de-acesso.html',
			controller: 'PerfisAcessoController'
		});

		$routeProvider.when('/', {
			templateUrl: 'partials/inicio.html'
		});

		$routeProvider.otherwise({redirectTo: '/'});

		$locationProvider.html5Mode(false);
	});
