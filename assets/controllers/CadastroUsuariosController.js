app.controller('CadastroUsuariosController', function($scope, $rootScope, $http, apiUrl, $cookies, $routeParams, $location) {
	
	$rootScope.MontarValidacao();

	$rootScope.usuario = {};
	$rootScope.perfisParaUsuarios = [];
	$scope.perfilSelecionado = {};
	$rootScope.repetirSenha = "";
	$scope.perfilAntigo = {};

	$scope.AlteraQuantidade = function(id){
		$http.get(apiUrl.QUANTIDADE + id).success(function() {

		}).error(function() {});
	}

	$rootScope.SalvarUsuario = function() {
		if($routeParams.id != undefined && $routeParams.id != 0) {
			$http.put(apiUrl.BUSCAR_PESSOAS + '/' + $routeParams.id, $rootScope.usuario).success(function(){
				$scope.AlteraQuantidade($scope.perfilSelecionado.id);
				$scope.AlteraQuantidade($scope.perfilAntigo.id);
				toastr.success('Usuário foi alterado com sucesso!');
				$location.path("/usuarios");
			}).error(function(){ toastr.success('Ocorreu um erro ao alterar o usuário!'); });			
		} else {
			$http.post(apiUrl.BUSCAR_PESSOAS, $rootScope.usuario).success(function(data){
				$rootScope.usuario = data;
				$scope.AlteraQuantidade($scope.perfilSelecionado.id);
				$scope.AlteraQuantidade($scope.perfilAntigo.id);
				toastr.success('Usuário foi salvo com sucesso!');
				$location.path("/usuarios");
			}).error(function(){ toastr.success('Ocorreu um erro ao salvar o usuário!'); });
		}
	}

	$rootScope.SelecionaPerfil = function() {
		$rootScope.usuario.perfil_id = angular.copy($scope.perfilSelecionado.id);
	}

	$rootScope.CancelarAlteracao = function() {
		$rootScope.usuario = {};
		$location.path("/usuarios");
	}

	$rootScope.CarregarPerfisUser = function() {
		$http.get(apiUrl.CARREGAR_PERFIS)
		.success(function(data) {
			$rootScope.perfisParaUsuarios = data;
		}).error(function() { toastr.success('Erro ao carregar os perfis de acesso.'); });
	}

	$rootScope.CarregarUsuario = function() {
		if($routeParams.id != undefined && $routeParams.id != 0) {
			$http.get(apiUrl.BUSCAR_PESSOAS + '/' + $routeParams.id).success(function(data) {
				$rootScope.usuario = data;
				$scope.perfilSelecionado.id = angular.copy(data.perfil_id);
				$scope.perfilAntigo = angular.copy($scope.perfilSelecionado);
			}).error(function() { toastr.success('Ocorreu um erro ao carregar os usuários.'); });
		}
	}

	function init() {
		$rootScope.CarregarPerfisUser();
		$rootScope.CarregarUsuario();
	}

	init();
});