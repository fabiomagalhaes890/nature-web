app.controller('MainController', function($scope, $http, $location, $rootScope, $routeParams, apiUrl, $cookies) {

	$rootScope.validar = {};
	$rootScope.momentViewPage = {};
	$rootScope.menu = [];
	$rootScope.usuarioLogado = "";
	$rootScope.enderecos = [];
	
	$rootScope.MontarValidacao = function(){
		$rootScope.validar.idUser = $cookies.get("idUser");
		$rootScope.validar.token = $cookies.get("token");

		if($rootScope.validar.idUser == undefined || $rootScope.validar.token == undefined) {
			$rootScope.momentViewPage.url = "partials/login.html"; 
			return;
		}

		$http.post(apiUrl.VALIDAR, $rootScope.validar).success(function(data) {		
			if(!data) { $rootScope.Logout(); }
			else { 
				$rootScope.nomeUser = $cookies.get("nome_usuario"); 
				$rootScope.momentViewPage.url = "partials/homepage.html";
				$location.path($location.path());
				$http.get(apiUrl.CARREGAR_ACESSOS_LOGIN + $cookies.get("idUser"))
				.success(function(res) {
					$rootScope.enderecos = res;
				}).error(function(){ alert("ERRO"); });
			}
		}).error(function(err) { console.log("Erro: " + err); });
	}

	$rootScope.Logout = function() {
		$rootScope.validar.idUser = $cookies.get("idUser");
		$http.post(apiUrl.LOGOUT, $rootScope.validar).success(function(data) {
			$cookies.remove("idUser");
			$cookies.remove("token");
			$cookies.remove("nome_usuario");
			$rootScope.enderecos = [];
			$rootScope.momentViewPage.url = "partials/login.html";
			$location.path("/");
		}).error(function(err) { console.log("Ocorreu um erro ao fazer logout."); });
	}

	$rootScope.RealizarLogin = function() {
		$http.post(apiUrl.LOGIN, $rootScope.userlogin).success(function(data) {
			if(data) {
				$cookies.put("idUser", data.idUser);
				$cookies.put("token", data.token);
				$cookies.put("nome_usuario", data.nomeUser);
				$rootScope.momentViewPage.url = "partials/homepage.html";
				$http.get(apiUrl.CARREGAR_ACESSOS_LOGIN + $cookies.get("idUser"))
				.success(function(res) {
					$rootScope.enderecos = res;
				}).error(function(){ alert("ERRO"); });

			} else { alert("Usuário ou senha incorreto(s)"); }
		}).error(function(data) { console.log("Erro ao realizar login"); });
	}
	
	$rootScope.MontarValidacao();
});