app.controller('PerfisAcessoController', function($scope, $http, apiUrl, $cookies, $rootScope) {
	
	$rootScope.MontarValidacao();

	$rootScope.perfis = [];
	$rootScope.acessos = [];
	$rootScope.perfil = {};
	$rootScope.myfilter = "";

	$rootScope.SelecionarPerfil = function(perfil) {
		$rootScope.perfil = angular.copy(perfil);
		$rootScope.acessos = [];
		$rootScope.CarregarAcessos();
		$("#myModal").modal("show");
	}

	$rootScope.NovoPerfil = function() {
		$rootScope.perfil = {};
		$rootScope.acessos = [];
		$rootScope.CarregarAcessos();
		$("#myModal").modal("show");
	}

	$rootScope.SelecionarParaRemover = function(perfil) {
		if(perfil.qtdUsuariosVinculados > 0) {
			toastr.success('O perfil não pode ser removido pois existem usuários vinculados!');
			return;
		}
		$rootScope.perfil = angular.copy(perfil);
		$("#remover-perfil").modal("show");
	}

	$rootScope.Cancelar = function() {
		$rootScope.perfil = {};
		$rootScope.acessos = [];
	}

	$rootScope.Remover = function() {
		if($rootScope.perfil.id != undefined && $rootScope.perfil.id != 0) {
			$http.delete(apiUrl.EXCLUIR_PERFIL + $rootScope.perfil.id)
			.success(function() {
				toastr.success('O perfil selecionado foi excluido com sucesso!');
				$rootScope.perfil = {};
				$rootScope.acessos = [];
				$("#remover-perfil").modal("hide");
			}).error(function(){
				toastr.success('Ocorreu um erro ao excluir o perfil selecionado');
			});
		}
		$rootScope.CarregarAcessos();
	}

	$rootScope.SalvarPerfil = function() {
		if($rootScope.perfil.nome == "" || $rootScope.perfil.nome == undefined) {
			toastr.success('Não é permitido criar um perfil sem nome!');
			return;
		}

		if($rootScope.perfil.id != undefined && $rootScope.perfil.id != 0) {
			$http.put(apiUrl.ALTERAR_PERFIL + $rootScope.perfil.id, $rootScope.perfil)
			.success(function() {				
				$rootScope.acessos.forEach(function(i) {
					var acesso = {
						id: i.id,
						id_menu: 0,
						id_perfil: 0,
						ativo: i.ativo
					};
					$http.post(apiUrl.SAVE_UPDATE_ACESSOS, acesso).success(function(data){ }).error(function(){});
				});
				toastr.success('Perfil alterado com sucesso!');
				$scope.CarregarPerfisDeAcesso();
				$("#myModal").modal("hide");
			}).error(function(){ toastr.success('Erro ao alterar o perfil de acesso.'); });
		} else {
			$rootScope.perfil.qtdUsuariosVinculados = 0;
			$http.post(apiUrl.SALVAR_PERFIL, $rootScope.perfil)
			.success(function(data) {
				$rootScope.acessos.forEach(function(i) {
					var acesso = {
						id: 0,
						id_menu: i.id,
						id_perfil: data.id,
						ativo: i.ativo
					};
					$http.post(apiUrl.SAVE_UPDATE_ACESSOS, acesso).success(function(data){ }).error(function(){});
				});
				toastr.success('Perfil salvo com sucesso!');
				$scope.CarregarPerfisDeAcesso();
				$("#myModal").modal("hide");
			}).error(function(){ toastr.success('Erro ao salvar o perfil de acesso.'); });
		}
	}

	$rootScope.CarregarAcessos = function() {
		$http.get(apiUrl.CARREGAR_ACESSOS + $rootScope.perfil.id)
		.success(function(data) {
			$rootScope.acessos = data;
		}).error(function() { toastr.success('Erro ao carregar os acessos do perfil.'); });
	}

	$scope.CarregarPerfisDeAcesso = function() {
		$http.get(apiUrl.CARREGAR_PERFIS)
		.success(function(data) {
			$rootScope.perfis = data;
		}).error(function() { toastr.success('Erro ao carregar os perfis de acesso.'); });
	}

	function init(){
		$scope.CarregarPerfisDeAcesso();
	}

	init();
});