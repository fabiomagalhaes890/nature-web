app.controller('ListagemUsuariosController', function($scope, $rootScope, $http, apiUrl, $cookies, $location) {
	
	$rootScope.MontarValidacao();

	$rootScope.usuarios = [];
	$rootScope.usuarioSelecionado = {};
	$scope.perfis = [];

	$scope.CarregarPerfisLista = function() {
		$http.get(apiUrl.CARREGAR_PERFIS)
		.success(function(data) {
			$scope.perfis = data;
			$rootScope.usuarios.forEach(function(us) {
				$scope.perfis.forEach(function(pe) {
					if(pe.id == us.perfil_id) {
						us.perfilusuario = pe.nome;
					}
				});
			});
		}).error(function() { toastr.success('Erro ao carregar os perfis de acesso.'); });
	}

	$rootScope.CancelarSelecaoUsuario = function(){
		$rootScope.usuarioSelecionado = {};
		$("#remover-usuario").modal("hide");
	}

	$rootScope.NovoUsuario = function() {
		$location.path("/usuarios/cadastro");
	}

	$rootScope.CarregarUsuarios = function() {
		$http.get(apiUrl.BUSCAR_PESSOAS).success(function(data) {
			$rootScope.usuarios = data;
			$scope.CarregarPerfisLista();
		}).error(function() { toastr.success('Ocorreu um erro ao carregar os usuários.'); });
	}

	$rootScope.SelecionarUsuario = function(usuario) {
		$location.path("/usuarios/cadastro/" + usuario.id);
	}

	$rootScope.SelecionarUsuarioParaRemover = function(usuario) {
		$rootScope.usuarioSelecionado = angular.copy(usuario);
		$("#remover-usuario").modal("show");
	}

	$rootScope.RemoverUsuario = function() {
		$http.delete(apiUrl.BUSCAR_PESSOAS + '/' + $rootScope.usuarioSelecionado.id).success(function(){
			$rootScope.CarregarUsuarios();
			$("#remover-usuario").modal("hide");
			toastr.success('Usuário removido com sucesso!');
		}).error(function() { toastr.success('Ocorreu um erro ao remover o usuário!'); });
	}

	function init() {
		$rootScope.CarregarUsuarios();
	}

	init();
});